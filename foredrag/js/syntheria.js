/**
** Script file for Syntheria
** version pre alpha, by Per-Øivin Berg Andersen (pan100@bitbucket)
** Changelog:
** 24.10.2015 Working sine synth with arpeggiator and simple sequencer.
**/

var syntheria = {
	synth : {
		oscillators : [],
		gains : [],
		mainGain : null,
		synthLoaded : null,
		activeNotes : [],
		arpeggiator : {
			active : false,
			currentNote : null,
			timeBetween : 25,
			timer : null,
			noteEvent : function(note, velocity) {
			//Skal inn i en event og this funker ikke
			arp = syntheria.synth.arpeggiator;
			if(velocity != 0) {
				if(syntheria.synth.activeNotes.indexOf(note) == -1) {
					syntheria.synth.activeNotes.push(note);
				}
			}
			else {
				syntheria.synth.activeNotes.splice(syntheria.synth.activeNotes.indexOf(note), 1);
			}
			if(syntheria.synth.activeNotes.length == 0) {
				clearInterval(arp.timer);
				arp.timer = null;
				arp.stopCurrentNote();
			}

			else {
				if(arp.timer == null) {
					arp.arpeggiate();
					arp.timer = setInterval(function() {
						arp.arpeggiate();
					},arp.timeBetween);
				}
			}
		},
		arpeggiate : function() {
			this.stopCurrentNote();
			syntheria.synth.activeNotes.sort();
			pos = syntheria.synth.activeNotes.indexOf(this.currentNote) + 1;
			if(pos > syntheria.synth.activeNotes.length-1) {
				pos = 0;
			}
			this.currentNote = syntheria.synth.activeNotes[pos];
			syntheria.synth.playMidiNote(this.currentNote,128);
			if(syntheria.sequencer.recording) {
				syntheria.sequencer.addNoteEvent(this.currentNote,128);
			}
		},
		stopCurrentNote : function() {
			if(this.currentNote != null) {
				syntheria.synth.playMidiNote(this.currentNote,0);
			}
			if(syntheria.sequencer.recording) {
				syntheria.sequencer.addNoteEvent(this.currentNote,0);
			}
		},
	errorMessages : {
		arpOff : "Sorry, the arpeggiator is not on."
	}
	},
	volumeController : {
		//can I send a signal to the midi keyboard asking for the initial volume?
		gain : 0.5,
		//EXPERIMENTAL
		realtimeSlider : false,
		realtimeSlide : function(newGain) {
			//EXPERIMENTAL
			if(!this.realtimeSlider) {
				throw "cannot slide in realtime without setting realtimeSlider to true first"
			}
			for(i = 1; i<129; i++) {
				current = syntheria.synth.gains[i].gain.value;
				if(!current == 0) {
					diffInGain = newGain - current;
					syntheria.synth.gains[i].gain.value = current + diffInGain;
				}
			}
			syntheria.synth.volumeController.gain = newGain;
		}
	},

	playMidiNote : function(noteNumber, volume) {
		if(this.synthLoaded != null) {
			this.gains[noteNumber].gain.value = volume / 128;
			console.log("gain " + this.gains[noteNumber].gain.value);
		}
		else {
			console.log("No synth is loaded");
		}
	},
	newOscillator : function(freq, type) {
		var oscillator = this.audioCtx.createOscillator();
		if (freq === undefined) freq = 440;
		if (type === undefined) type = 'sawtooth';
		oscillator.type = type;
		oscillator.frequency.value = freq;
		return oscillator;		
	},
	midiToFrequency : function(noteNumber, a4Frequency) {
		if (a4Frequency === undefined) a4Frequency = 440;
		//Note 69 er A4 i MIDI
		return a4Frequency * Math.pow(2, (noteNumber-69)/12);
	},
	onMIDISuccess : function(midiAccess) {
		this.midi = midiAccess;
		var inputs = this.midi.inputs.values();
    	// loop over all available inputs and listen for any MIDI input
    	for (var input = inputs.next(); input && !input.done; input = inputs.next()) {
        	// each time there is a midi message call the onMIDIMessage function
        	//figure out why we cant use this here
        	input.value.onmidimessage = syntheria.synth.onMIDIMessage;
        }

        this.midi.onstatechange = this.onStateChange;
    },
    onMIDIMessage : function(event) {
    	data = event.data,
    	cmd = data[0] >> 4,
    	channel = data[0] & 0xf,
    	type = data[0] & 0xf0,
    	note = data[1],
    	velocity = data[2];
		//if(data[0] !== 248) {

		//}
			console.log('MIDI data', data);
			console.log('Command ' + cmd);
			console.log('channel ' + channel);
			console.log('type ' + type);
			console.log('note ' + note);
			console.log('velocity ' + velocity);

		if(note!=null && cmd===9) {
			adjustedVelocity = velocity * syntheria.synth.volumeController.gain;
			//again, this doesnt work here must adress synth object
			if(syntheria.synth.arpeggiator.active) {
				syntheria.synth.arpeggiator.noteEvent(note, adjustedVelocity);
			}
			else {
				syntheria.synth.playMidiNote(note, adjustedVelocity);
				if(syntheria.sequencer.recording) {
					syntheria.sequencer.addNoteEvent(note, adjustedVelocity);
				}	
			}
		}
		if(cmd===11) {
			if(syntheria.synth.volumeController.realtimeSlider) {
				syntheria.synth.volumeController.realtimeSlide(velocity);
			}
			else {
				syntheria.synth.volumeController.gain = velocity / 128;			
			}
		}
	},
	onStateChange : function(event) {
		//not implemented
	},
	midiFailMessage : "No access to MIDI devices or your browser doesn't support WebMIDI API. Should work with WebMIDIAPIShim perhaps",
	onMIDIFailure : function(e) {
		console.log(this.midiFailMessage + e);
	},
	loadSineSynth : function() {
		if(this.synthLoaded) {
			this.unloadSynth();
		} 
		for(i = 1; i<129; i++) {
			syntheria.synth.oscillators[i] = syntheria.synth.newOscillator(syntheria.synth.midiToFrequency(i),'sine');
			syntheria.synth.gains[i] = syntheria.synth.audioCtx.createGain();
			syntheria.synth.oscillators[i].connect(syntheria.synth.gains[i]);
			syntheria.synth.gains[i].connect(syntheria.synth.mainGain);
			syntheria.synth.mainGain.connect(syntheria.synth.audioCtx.destination);
			syntheria.synth.gains[i].gain.value = 0.0;
			syntheria.synth.oscillators[i].start();
		}
		syntheria.synth.synthLoaded = "basic sine";
	},
	loadFirstAdditive : function() {
		if(this.synthLoaded) {
			this.unloadSynth();
		} 
		for(i = 1; i<129; i++) {
			syntheria.synth.oscillators[i] = Array();
			freq = syntheria.synth.midiToFrequency(i);

			syntheria.synth.oscillators[i][0] = syntheria.synth.newOscillator(freq,'sine');
			syntheria.synth.oscillators[i][1] = syntheria.synth.newOscillator(freq + ((freq/100)*0.5),'sine');
			syntheria.synth.oscillators[i][0].start();
			syntheria.synth.oscillators[i][1].start();

			pregainForSaw = syntheria.synth.audioCtx.createGain();
			pregainForSaw.gain.value = 0.6;
			syntheria.synth.oscillators[i][1].connect(pregainForSaw);

			syntheria.synth.gains[i] = syntheria.synth.audioCtx.createGain();
			syntheria.synth.oscillators[i][0].connect(syntheria.synth.gains[i]);
			syntheria.synth.gains[i].connect(syntheria.synth.mainGain);
			syntheria.synth.mainGain.connect(syntheria.synth.audioCtx.destination);
			syntheria.synth.gains[i].gain.value = 0.0;

			pregainForSaw.connect(syntheria.synth.gains[i]);
		}
		syntheria.synth.synthLoaded = "first additive synth";		
	},
	unloadSynth : function() {
		//unload all oscillators
		console.log("length of oscillators is " + this.oscillators.length);
		for (var i = 1; i <= this.oscillators.length-1; i++) {
			console.log("about to stop oscillator " + i);
			console.log(this.oscillators[i]);
			if(this.oscillators[i] instanceof Array) {
				console.log(this.oscillators[i].length);
				for (var j = 0; j <= this.oscillators[i].length -1; j++) {
					console.log("that's an array");
					syntheria.synth.oscillators[i][j].stop();
				};
			}
			else {
				console.log("stopping oscillator " + syntheria.synth.oscillators[i]);
				syntheria.synth.oscillators[i].stop();
			}
		};
		this.oscillators = [];
		this.gains = [];
		this.synthLoaded = null;
	},
	hookupMIDI : function() {
		if (navigator.requestMIDIAccess) {
			navigator.requestMIDIAccess({
				//TODO what is SYSEX?
				sysex: false
			}).then(this.onMIDISuccess, this.onMIDIFailure);
		} else {
			throw this.midiFailMessage;
			alert(this.midiFailMessage);
		}
	},
	initMainGain : function() {
		this.mainGain = this.audioCtx.createGain();
		this.mainGain.gain.value = 1.0;
	},
	init : function() {
		this.initMainGain();
		this.hookupMIDI();
		//this.loadSineSynth();
	}
},
sequencer : {
	model : {
		NoteEvent : function(timestamp, note, velocity) {
			this.timestamp = timestamp;
			this.note = note;
			this.velocity = velocity;
			return this;
		},
		Song : function(firstTimeMS, name) {
			this.firstTimeMS = firstTimeMS;
			this.noteEvents = [];
			this.name = name;
			this.isEmpty = function() {
				if(this.noteEvents.length == 0) {
					return true;
				}
				return false;
			}
			return this;
		}
	},
	recording : false,
	playing: false,
	activeSong : null,
	bank : [],
	startRecording : function(waitForInput) {
		this.recording = true;
		if(waitForInput === undefined || waitForInput == false) {
			this.activeSong = new this.model.Song();
		}
		else {
			this.activeSong = new this.model.Song(Date.now());
		}
	},
	stopRecording : function() {
		this.recording = false;
	},
	addNoteEvent : function(note, velocity) {
		if(this.activeSong == null) {
			throw this.errorMessages.noSong;
		}
		this.activeSong.noteEvents.push(new this.model.NoteEvent(Date.now(), note, velocity));
	},
	playActiveSong : function() {
		if(this.activeSong == null) {
			throw this.errorMessages.noSong;
		}
		this.playSong(this.activeSong);
	},
	playSong : function(song) {
		for (var i = 0; i <= this.activeSong.noteEvents.length - 1; i++) {
				if(song.firstTimeMS == undefined) {
					song.firstTimeMS = song.noteEvents[0].timestamp;
				}
				//for every tone set a timeout and play it on the synth
				console.log(i);
				toneFunction = this.getToneFunction(song.noteEvents[i].note, song.noteEvents[i].velocity);
				setTimeout(toneFunction, song.noteEvents[i].timestamp - song.firstTimeMS);
			};	
	},
	getToneFunction : function(note, velocity) {
		return function() {
			syntheria.synth.playMidiNote(note, velocity);
		}
	},
	saveSongToBank : function() {
		this.bank.push(this.activeSong);
	},
	errorMessages : {
		noSong : "Sorry, there is no active song. Call startRecording() first"
	}
}
}

syntheria.synth.audioCtx = new (window.AudioContext || window.webkitAudioContext)();
window.onbeforeunload = function() {
	synth.audioCtx.close();
}