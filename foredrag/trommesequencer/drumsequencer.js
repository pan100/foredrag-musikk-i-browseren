	var log = console.log.bind(console);
	var ctx = new(window.AudioContext || window.webkitAudioContext)();

	var drumboxModule = angular.module('drumboxModule', []);

	drumboxModule.controller('DrumboxController', ['$scope', '$http', function($scope, $http) {
		$scope.sounds = [];
		$scope.bpm = 120;
		$scope.frames = 16;
		$scope.playing = false;
		$scope.startBox = function() {
			if(!$scope.playing) {
				$scope.playing = true;
				$scope.stepBox(0);
			}

		};
		$scope.stopBox = function() {
			$scope.playing = false;
		}
		$scope.stepBox = function(frame) {
			$scope.sounds.forEach(function(sound) {
				if(sound.slots[frame].value) {
					sound.play();
				}
			});
			
			if($scope.playing) {
				var nextFrame = frame+1;
				if(nextFrame >= $scope.frames) {
					nextFrame = 0;
				}
				log(nextFrame);
				//TODO requestAnimFrame?
				setTimeout(function() {$scope.stepBox(nextFrame)}, 60000/$scope.bpm/4);
			}
		};


		var soundFileNames = ['cl_hihat', 'claves', 'conga1', 'cowbell', 'crashcym', 'handclap', 'hi_conga', 'hightom', 'kick1', 'kick2', 'maracas', 'open_hh', 'rimshot', 'snare', 'tom1'];
		soundFileNames.forEach(function(filename) {
			var url = 'trommesequencer/drumkit/' + filename + '.mp3';
			$http.get(url, {responseType : 'arraybuffer'}).success(function(data, status, headers, config) {
				ctx.decodeAudioData(data, function(buffer) {
					var soundObject = {
						name : filename,
						sound: buffer,
						play : function() {
							var source = ctx.createBufferSource();
							source.buffer = this.sound;
							source.connect(ctx.destination);
							source.start(0);
						}
					}
					soundObject.slots = [];
					for (var i = 1; i <= $scope.frames; i++) {
						var slot = {
							number : i,
							value : false
						}
						soundObject.slots.push(slot);
					};
					$scope.sounds.push(soundObject);
					$scope.$apply();
				}, function(err) {
					log(err);
				});
			}).error(function(data, status, headers, config) {
				log("error status " + status);
			});
		});
	}]);